#include <cvx/renderer/scene.hpp>
#include <cvx/renderer/renderer.hpp>
#include <cvx/renderer/viewpoint_sampler.hpp>
#include <cvx/util/geometry/util.hpp>
#include <cvx/util/misc/strings.hpp>

using namespace std ;
using namespace Eigen ;
using namespace cvx::renderer ;
using namespace cvx::util ;

int main(int argc, char *argv[])
{
    ViewPointSampler sampler ;
    vector<Matrix4f> views;
    sampler.setRadius(1.0, 1.0, 0.1);
    sampler.setCenter(Vector3f(0, 0, 0)) ;
//    sampler.setRoll(-M_PI/12, M_PI/12, M_PI/20);
    sampler.generate(300, views);

 //   ScenePtr s ;
 //   s = Scene::load("/home/malasiot/Downloads/kuka-kr5-r850.zae") ;
  //  s = Scene::load("/home/malasiot/Downloads/human.dae") ;
//    s = Scene::load("/home/malasiot/Downloads/box.dae") ;

    //ScenePtr s = Scene::loadAssimp("/home/malasiot/Downloads/20objects/data/Kinfu_Audiobox1_light/object.obj") ;
    //ScenePtr s = Scene::load("/home/malasiot/tmp/table.dae") ;
    ScenePtr s = Scene::loadAssimp("/home/malasiot/ramcip_ws/src/ramcip_certh/certh_object_recognition/meshes/amita.ply") ;

 //  s->addLight(LightPtr(new DirectionalLight(Vector3f(0, 0, -1), Vector3f(0.5, 0.5, 0.5)))) ;
   s->addLight(LightPtr(new DirectionalLight(Vector3f(0, 0, 1), Vector3f(0.5, 0.5, 0.5)))) ;
    SceneRenderer rdr(s, getOffscreenContext(640, 480)) ;
    rdr.setBackgroundColor(Vector4f(0, 0.5, 1, 1));

    //    PerspectiveCamera pcam(PinholeCamera(550, 550, 640/2.0, 480/2.0, cv::Size(640, 480)),
      //                         lookAt(Vector3f(0, 0.5, 4), Vector3f(0, 0.5, 0), Vector3f(0, 1, 0))) ;

    for( uint i=0 ; i<views.size() ; i++ ) {
        PerspectiveCamera pcam(PinholeCamera(550, 550, 640/2.0, 480/2.0, cv::Size(640, 480)), views[i]) ;
        rdr.render(pcam, SceneRenderer::RENDER_GOURAUD) ;

        cv::imwrite(format("/tmp/view%05d.png", i), rdr.getColor()) ;

    }

    ViewPointSampler::exportCameras("/tmp/cameras.dae", views, PerspectiveCamera(PinholeCamera(550, 550, 640/2.0, 480/2.0, cv::Size(640, 480)))) ;

  //  cv::imwrite("/tmp/oo.png", rdr.getColor()) ;

}
