#include "scene_loader.hpp"

#include <vector>

//#include "collada_loader.hpp"
#include "obj_loader.hpp"

#include <cvx/util/misc/path.hpp>
#include <cvx/util/misc/strings.hpp>

using namespace std ;
using namespace cvx::util ;

SceneLoaderRegistry::SceneLoaderRegistry() {
    driver_list_.push_back(SceneLoaderPtr(new OBJSceneLoader())) ;
}

SceneLoaderPtr SceneLoader::findDriver(const std::string &name)
{
    const vector<SceneLoaderPtr> &drivers = SceneLoaderRegistry::instance().driver_list_ ;
    for ( uint i=0 ; i<drivers.size() ; i++ )
        if ( drivers[i]->canLoad(name) ) return drivers[i] ;

    return SceneLoaderPtr() ;
}

SceneLoaderPtr SceneLoader::findByFileName(const std::string &fname)
{
    string fext = Path(fname).extension() ;

    if ( fext.empty() ) return SceneLoaderPtr() ;

    toLower(fext) ;

    const vector<SceneLoaderPtr> &drivers = SceneLoaderRegistry::instance().driver_list_ ;

    for ( uint i=0 ; i<drivers.size() ; i++ ) {
        SceneLoaderPtr d = drivers[i] ;

        string extensions = d->getExtensions() ;

        vector<string> tokens = split(extensions, ";") ;
        if ( std::find(tokens.begin(), tokens.end(), fext) != tokens.end() ) return d ;
    }

    return SceneLoaderPtr() ;
}
